/*
 * ZZZip/include/zzz.h
 * https://gitlab.com/bztsrc/zzzip
 *
 * Copyright (C) 2021 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief ZZZip library API
 *
 */

#ifndef _ZZZ_H_
#define _ZZZ_H_

#ifdef  __cplusplus
extern "C" {
#endif

#include <stdint.h>

/* file format and API version */
#define ZZZ_VERSION      0x00

#ifndef _MSC_VER
#define _inline __inline__
#define _pack __attribute__((packed))
#define _unused __attribute__((unused))
#pragma pack(push)
#pragma pack(1)
#else
#define _inline
#define _pack
#define _unused __pragma(warning(suppress:4100))
#endif

/* file magics */
#define ZZZ_MAGIC   "ZZz\032"   /* magic for encryption or entity blocks */
#define ZZZ_MAGEND  "ZEnd"      /* magic for terminating end block */

/* encryption methods */
enum {
    ZZZ_ENC_AES_256_CBC,        /* military grade NIST AES */
    ZZZ_ENC_SHA_256_XOR,        /* simple symmetric xor cipher */
    ZZZ_ENC_LAST
};

/* encryption block */
typedef struct {
    uint8_t magic[4];           /* magic bytes, ZZZ_MAGIC same as in entity block */
    uint16_t size;              /* size of the block, indirectly defines number of encryption filters used */
    uint16_t mbz;               /* file name length, must be zero */
    uint32_t passwdcrc;         /* CRC of the password */
    uint8_t padding;            /* content padding in power of 2 */
    uint8_t enctype[256];       /* encryption method(s) (could be more, but there must be at least one) */
} _pack zzz_enc_block_t;

/* OS-independent file types */
#define ZZZ_TYPE_REG    0x00    /* regular file */
#define ZZZ_TYPE_LNK    0x10    /* hard link */
#define ZZZ_TYPE_SYM    0x20    /* symbolic link */
#define ZZZ_TYPE_UNI    0x30    /* directory union */
#define ZZZ_TYPE_CHR    0x40    /* character device */
#define ZZZ_TYPE_BLK    0x50    /* block device */
#define ZZZ_TYPE_DIR    0x60    /* directory */
#define ZZZ_TYPE_FIFO   0x70    /* named FIFO */
#define ZZZ_TYPE_LAST   0x80
#define ZZZ_FILE_TYPE(x)    ((x) & 0xF0)    /* use on zzz_block_t.type */
#define ZZZ_NUM_FILTER(x)   ((x) & 0x0F)    /* use on zzz_block_t.type */
/* steal one bit from file name size and add it to header size */
#define ZZZ_HEADER_MAXSIZE 131072
#define ZZZ_HEADER_SIZE(x) (int)(((((zzz_block_t*)(x))->namelen & 0x8000) << 1) | ((zzz_block_t*)(x))->size)
#define ZZZ_FILENAME_LEN(x) (int)(((zzz_block_t*)(x))->namelen & 0x7FFF)

/* entity block header */
typedef struct {
    uint8_t magic[4];           /* magic bytes, ZZZ_MAGIC */
    uint16_t size;              /* size of the header */
    uint16_t namelen;           /* file name length, must not be zero */
    uint16_t myear;             /* file modification year */
    uint8_t mmon;               /* file modification month 1 - 12 */
    uint8_t mday;               /* file modification day 1 - 31 */
    uint8_t mhour;              /* file modification hour 0 - 23 */
    uint8_t mmin;               /* file modification minute 0 - 59 */
    uint8_t msec;               /* file modification second 0 - 60 */
    uint8_t type;               /* block type, file type and number of filters */
    uint64_t uncompressed;      /* uncompressed file size */
    int64_t uncompressed_hi;
    uint64_t contentsize;       /* content size or compressed file size (without optional padding) */
    int64_t contentsize_hi;
} _pack zzz_block_t;
extern char c_assert1[sizeof(zzz_block_t) == 48 ? 1 : -1];

/* filters */
enum {
    ZZZ_FILTER_ASCII,           /* newline conversion */
    ZZZ_FILTER_CODE,            /* bcj executable code conversion */
    ZZZ_FILTER_ZLIB,            /* zlib deflate */
    ZZZ_FILTER_BZIP2,           /* bzip2 */
    ZZZ_FILTER_LZMA,            /* lzma */
    ZZZ_FILTER_XZ,              /* lzma2, xz */
    ZZZ_FILTER_PPMD,            /* PPMdH, similar to rar */
    ZZZ_FILTER_ZSTD             /* ZStandard */
};

/* one filter descriptor */
typedef struct {
    uint8_t method;             /* one of the ZZZ_FILTER_x define above */
    uint8_t level;              /* compression level, filter specific */
} _pack zzz_filter_t;

/* extra field types */
enum {
    ZZZ_EXTRA_PAD = 0,          /* padding */
    ZZZ_EXTRA_COMMENT,          /* UTF-8 comment */
    ZZZ_EXTRA_MIME,             /* mime type, with optional ";charset=x" */
    ZZZ_EXTRA_ICON,             /* PNG or SVG icon */
    ZZZ_EXTRA_META,             /* meta info (XAttr) */
    ZZZ_EXTRA_TIME,             /* POSIX timestamps */
    ZZZ_EXTRA_ACCESS,           /* POSIX mode, uid, gid, username and groupname */
    ZZZ_EXTRA_NFSACL,           /* POSIX NFSv4 ACL */
    ZZZ_EXTRA_TEXTACL,          /* text-based ACL (OS/2 and POSIX 1003e draft std 17) */
    ZZZ_EXTRA_UUIDACL,          /* UUID ACL */
    /* other, OS-Specific information */
    ZZZ_EXTRA_OS_OS2 = 0x10,
    ZZZ_EXTRA_OS_VMS,
    ZZZ_EXTRA_OS_MAC,
    ZZZ_EXTRA_OS_WIN,
    ZZZ_EXTRA_OS_T4,
    ZZZ_EXTRA_OS_OS390,
    ZZZ_EXTRA_OS_AS400,
    ZZZ_EXTRA_OS_MORPH,
    ZZZ_EXTRA_OS_HAIKU
};

/* extra field descriptor */
typedef struct {
    uint8_t type;               /* one of the ZZZ_EXTRA_x define above */
    uint8_t rev;                /* most significant bit means big-endian for OS-Specific fields */
    uint16_t size;              /* size of the extra field */
} _pack zzz_extra_t;

/* end block */
typedef struct {
    uint8_t magic[4];           /* magic bytes, ZZZ_MAGEND */
    uint16_t size;              /* size of the block, always 48 */
    uint16_t filetypes;         /* bitmask for stored file types */
    uint16_t ayear;             /* archive creation year */
    uint8_t amon;               /* archive creation month 1 - 12 */
    uint8_t aday;               /* archive creation day 1 - 31 */
    uint8_t ahour;              /* archive creation hour 0 - 23 */
    uint8_t amin;               /* archive creation minute 0 - 59 */
    uint8_t asec;               /* archive creation second 0 - 60 */
    uint8_t revision;           /* ZZZ_VERSION */
    uint64_t uncompressed;      /* total of uncompressed sizes in entity blocks */
    int64_t uncompressed_hi;
    uint64_t numblk;            /* total number of blocks in the archive */
    uint32_t filters;           /* bitmask of used filters */
    uint32_t acrc;              /* archive CRC */
} _pack zzz_end_t;
extern char c_assert2[sizeof(zzz_end_t) == 48 ? 1 : -1];

#ifdef _MSC_VER
#pragma pack(pop)
#endif

/* in-memory meta descriptor */
typedef struct {
    uint16_t size;              /* data size */
    char *name;                 /* UTF-8 key */
    uint8_t *data;              /* binary data */
} zzz_meta_t;

/* in-memory extra field descriptor */
typedef struct {
    uint8_t type;               /* one of the ZZZ_EXTRA_x define above */
    uint8_t rev;                /* most significant bit means big-endian for OS-Specific fields */
    uint16_t size;              /* size of the the extra field's data */
    union {
        uint8_t *any;           /* used for more types */
        struct {
            uint16_t len;       /* number of meta key-value pairs */
            zzz_meta_t *data;   /* key-value pairs */
        } meta;
        struct {
            uint64_t mtime;     /* nanosec modification timestamp */
            uint64_t atime;     /* nanosec access timestamp */
            uint64_t ctime;     /* nanosec status change timestamp */
            uint64_t btime;     /* nanosec birth (creation) timestamp */
        } time;
        struct {
            uint32_t mode;      /* POSIX access right bits */
            uint64_t uid;       /* owner numeric id */
            uint64_t gid;       /* group numeric id */
            char *user;         /* owner name */
            char *group;        /* group name */
        } access;
    } attr;
} zzz_attr_t;

/* in-memory entity */
typedef struct {
    zzz_block_t header;         /* copy of the block header, machine native endian */
    zzz_filter_t filter[16];    /* used compression filters */
    char *filename;             /* filename */
    char *target;               /* target for links */
    int numextra;               /* number of extra fields */
    zzz_attr_t *extra;          /* extra fields */
    uint32_t devmaj, devmin;    /* device major and minor for device files */
    uint32_t origcrc;           /* uncompressed file's CRC */
    uint32_t blkcrc;            /* entity block's CRC */
} zzz_entity_t;

/* return codes */
enum {
    ZZZ_OK,                     /* 0 success */
    ZZZ_ENCRYPTED,              /* 1 got an encryption block when reading */
    ZZZ_END,                    /* 2 last block reached */
    ZZZ_ERR_BADINP,             /* 3 bad input parameter */
    ZZZ_ERR_NOMEM,              /* 4 not enough memory */
    ZZZ_ERR_HDRTOOBIG,          /* 5 header too big (max 128k) */
    ZZZ_ERR_IO,                 /* 6 file input output error */
    ZZZ_ERR_CORRUPT,            /* 7 corrupt file format or bad CRC */
    ZZZ_ERR_UNSUPPORTED         /* 8 unsupported filter/type in archive */
};

#define ZZZ_BUFSIZE 1048576     /* read and write buffer size (1M) */

/* parameter for zzz_entity_file's text argument */
enum {
    ZZZ_BINARY,
    ZZZ_TEXT
};

/* common.c */
uint32_t zzz_crc32(uint32_t crc, uint8_t *buf, uint64_t len);
char *zzz_realpath(const char *path);
int zzz_entity_extra(void *ctx, int type, uint8_t *buf, uint16_t size);
void zzz_entity_free(zzz_entity_t *ent);

/* create.c */
void *zzz_create_file(char *fn);
void *zzz_create_stream(int fd);
void *zzz_create_mem();
int zzz_compression(void *ctx, int method, int level);
int zzz_encrypt(void *ctx, int method, uint8_t *password, int pwdlen);
int zzz_entity_file(void *ctx, char *fn, int text, int64_t size);
int zzz_entity_hardlink(void *ctx, char *fn, char *target);
int zzz_entity_symlink(void *ctx, char *fn, char *target);
int zzz_entity_union(void *ctx, char *fn);
int zzz_entity_union_target(void *ctx, char *target);
int zzz_entity_chrdev(void *ctx, char *fn, uint32_t devmaj, uint32_t devmin);
int zzz_entity_blkdev(void *ctx, char *fn, uint32_t devmaj, uint32_t devmin);
int zzz_entity_dir(void *ctx, char *fn);
int zzz_entity_fifo(void *ctx, char *fn);
int zzz_entity_mtime(void *ctx, uint16_t year, uint8_t mon, uint8_t day, uint8_t hour, uint8_t min, uint8_t sec);
int zzz_entity_mtime_t(void *ctx, uint64_t t);
int zzz_entity_extra_comment(void *ctx, char *comment);
int zzz_entity_extra_mime(void *ctx, char *mime);
int zzz_entity_extra_icon(void *ctx, uint8_t *buf, uint16_t size);
int zzz_entity_extra_meta(void *ctx, char *key, uint8_t *value, uint16_t size);
int zzz_entity_extra_time(void *ctx, uint64_t mtime, uint64_t atime, uint64_t ctime, uint64_t btime);
int zzz_entity_extra_access(void *ctx, uint32_t mode, char *user, uint64_t uid, char *group, uint64_t gid);
int zzz_entity_extra_acl_nfs(void *ctx, char *acl);
int zzz_entity_extra_acl_text(void *ctx, char *acl);
int zzz_entity_extra_acl_uuid(void *ctx, uint8_t *acl, uint16_t size);
int zzz_entity_extra_os_tag(void *ctx, int ostype, uint16_t tag, uint8_t *buf, uint16_t size);
int zzz_entity_data(void *ctx, uint8_t *buf, uint64_t size);
int zzz_entity_flush(void *ctx);
int zzz_finish(void *ctx, uint8_t **mem, uint64_t *size);

/* extract.c */
void *zzz_open_file(char *fn);
void *zzz_open_stream(int fd);
void *zzz_open_mem(uint8_t *mem, uint64_t size);
int zzz_decrypt(void *ctx, uint8_t *password, int pwdlen);
int zzz_read_header(void *ctx, zzz_entity_t **ent);
int zzz_read_data(void *ctx, uint8_t *buf, uint64_t *size);
int zzz_close(void *ctx);

#ifdef  __cplusplus
}
#endif

#endif /* _ZZZ_H_ */
