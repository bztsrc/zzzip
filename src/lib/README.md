ZZZip Archive Format SDK
========================

This library provides functions to create and extract ZZZip archives. For a complete manual, check out the
[API documentation](https://gitlab.com/bztsrc/zzzip/-/blob/master/docs/API.md).

Compilation
-----------

To compile both archive creation and extraction into the library, do
```sh
$ make
```

To compile only archive creation:
```sh
$ ZZZ_CREATEONLY=1 make
```

To compile only archive extraction:
```sh
$ ZZZ_EXTRACTONLY=1 make
```

Quick Guide: Creating an Archive
--------------------------------

```c
/* create the archive */
void *zzz = zzz_create_file("archive.zzz");

/* set up compression on entire archive (without the entities will be compressed individually) */
zzz_compression(zzz, ZZZ_METHOD_ZSTD, 5);

/* add an entity, text file with NL conversion */
zzz_entity_file(zzz, "file1.txt", ZZZ_TEXT, 123);
zzz_entity_data(zzz, &data, 123);
zzz_entity_flush(zzz);

/* add another, a binary file this time */
zzz_entity_file(zzz, "file2.jpg", ZZZ_BINARY, 8192);
zzz_entity_extra_mime(zzz, "image/jpeg");
zzz_entity_data(zzz, &data +    0, 4096);
zzz_entity_data(zzz, &data + 4096, 4096);
zzz_entity_flush(zzz);

/* add a symlink */
zzz_entity_symlink(zzz, "link.txt", "file1.txt");
zzz_entity_flush(zzz);

/* add a directory */
zzz_entity_dir(zzz, "dir/");
zzz_entity_extra_access(zzz, 0755, "guest", 1000, "users", 100);
zzz_entity_flush(zzz);

/* close the archive and free resources */
zzz_finish(zzz, NULL, NULL);
```

Quick Guide: Extracting an Archive
-----------------------------------

```c
zzz_entity_t *ent;
/* open an archive for reading */
void *zzz = zzz_open_file("archive.zzz");

/* read the next entity from file */
switch(zzz_read_header(zzz, &ent)) {
    case ZZZ_END:
        /* end of archive, everything was fine and went well */
        break;

    case ZZZ_ENCRYPTED:
        /* the first entity can be an encryption record. If so, set the password */
        zzz_decrypt(zzz, password, strlen(password));
        break;

    case ZZZ_OK:
        /* we got an entity */
        switch(ZZZ_FILE_TYPE(ent->header.type)) {

            case ZZZ_TYPE_REG:
                /* read data */
                zzz_read_data(zzz, buf, &ent->header.uncompressed);
                /* save the file */
                f = fopen(pathconcat(targetdir, ent->filename), "wb");
                fwrite(buf, ent->header.uncompressed, 1, f);
                fclose(f);
                break;

            case ZZZ_TYPE_SYM:
                /* symbolic link */
                symlink(ent->target, pathconcat(targetdir, ent->filename));
                break;

            case ZZZ_TYPE_DIR:
                /* directory */
                mode = 0755;
                for(i = 0; i < ent->numextra; i++)
                    if(ent->extra[i].type == ZZZ_EXTRA_ACCESS) {
                        mode = ent->extra[i].attr.access.mode;
                        break;
                    }
                mkdir(pathconcat(targetdir, ent->filename), mode);
                break;

            default:
                /* there are other types, like devices, fifo etc. */
                break;
        }
        break;

    default:
        /* anything else is an error code ZZZ_ERR_x */
        break;
}
/* close the archive and free resources */
zzz_close(zzz);
```
