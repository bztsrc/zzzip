#
#  zzz/src/lib/Makefile
#
#  Copyright (C) 2021 bzt (bztsrc@gitlab)
#
#  Permission is hereby granted, free of charge, to any person
#  obtaining a copy of this software and associated documentation
#  files (the "Software"), to deal in the Software without
#  restriction, including without limitation the rights to use, copy,
#  modify, merge, publish, distribute, sublicense, and/or sell copies
#  of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be
#  included in all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
#  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
#  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#  DEALINGS IN THE SOFTWARE.
#
#  @brief Project makefile
#

TARGET = ../../lib/libzzz.a

CFLAGS = -ansi -pedantic -Wall -Wextra -Wno-implicit-fallthrough -Wno-long-long -O3 -I./ -I./zstd -I../../include \
	-D_FILE_OFFSET_BITS=64 -D__USE_FILE_OFFSET64 -D__USE_LARGEFILE

ifneq ($(ZZZ_CREATEONLY),)
SRCS = common.c create.c crc32.c sha.c aes.c
OBJS = zstd/compress/*.o
CFLAGS += -DNOEXTRACT=1
else
ifneq ($(ZZZ_EXTRACTONLY),)
SRCS = common.c extract.c crc32.c sha.c aes.c zlib.c bzip2.c xz.c
OBJS = zstd/decompress/*.o
CFLAGS += -DNOCREATE=1
else
SRCS = $(wildcard *.c)
OBJS = zstd/decompress/*.o zstd/compress/*.o
endif
endif
OBJS += zstd/common/*.o $(SRCS:.c=.o)

ifeq ($(DEBUG),1)
CFLAGS += -DDEBUG=1 -g
endif
CC ?= gcc
AR ?= ar

all: $(TARGET)

zstd/libzstd.a:
	@make -C zstd libzstd.a ZSTD_LEGACY_SUPPORT=0 ZSTD_LIB_DICTBUILDER=0 ZSTD_LIB_DEPRECATED=0 \
		ZSTD_STATIC_LINKING_ONLY=1 ZSTD_STRIP_ERROR_STRINGS=1 DEBUGLEVEL=0

%: %.c
	$(CC) $(CFLAGS) $< -c $@

$(TARGET): zstd/libzstd.a $(OBJS)
	@mkdir ../../lib 2>/dev/null || true
	@rm $@ 2>/dev/null || true
	$(AR) -frsv $@ $(OBJS) >/dev/null

clean:
	@rm $(TARGET) *.o 2>/dev/null || true
	@rmdir ../../lib 2>/dev/null || true

distclean: clean
	@make -C zstd clean || true

