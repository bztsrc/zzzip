#include <zzz.h>
#include <zstd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <endian.h>
#include <unistd.h>
#ifndef NOEXTRACT
#include "zlib.h"
#include "bzip2.h"
#include "xz.h"
#endif

#if __BYTE_ORDER == __LITTLE_ENDIAN
#define le16(x) (x)
#define le32(x) (x)
#define le64(x) (x)
#else
#define le16(x) __builtin_bswap16(x)
#define le32(x) __builtin_bswap32(x)
#define le64(x) __builtin_bswap64(x)
#endif

#ifdef __WIN32__
#define NL 2
#else
#define NL 1
#endif
#ifdef _OS_Z_
#define zzz_file_t fid_t
#else
#define zzz_file_t FILE*
#endif

#define MAX_ENC_FILTERS 8

typedef struct {
    zzz_enc_block_t enc;
    zzz_filter_t comp;
    zzz_file_t f;
    int fd;
    uint8_t *mem, *hdr, *cmp, *fcmp, *nl, oneround, enchdr;
    zzz_end_t end;
    zzz_entity_t entity;
    uint32_t cmpcrc, hdrsize;
    uint64_t pos, apos, hpos, mpos;
#ifndef NOCREATE
    ZSTD_CCtx *ae, *ee;
    ZSTD_outBuffer zo;
#endif
#ifndef NOEXTRACT
    ZSTD_DCtx *ad, *ed;
    z_stream az, ez;
    bz_stream ab, eb;
    struct xz_buf axd, exd;
    struct xz_dec *ax, *ex;
    ZSTD_inBuffer azi, ezi;
    ZSTD_outBuffer azo, ezo;
#endif
    int pad;
    uint32_t rd[4*15], erd[4*15], drd[4*15];
    uint8_t iv[16], ivec[MAX_ENC_FILTERS][16], chunk[128], chksiz;
    uint8_t shb, shx, shm[32], shv[MAX_ENC_FILTERS][33];
} zzz_ctx_t;

/* common.c */
zzz_file_t zzz_fopen(char *fn, int wr);
int64_t zzz_fwrite(zzz_file_t f, void *buf, int64_t size);
int64_t zzz_fread(zzz_file_t f, void *buf, int64_t size);
int zzz_fseek(zzz_file_t f, int64_t offs);
uint64_t zzz_filesize(zzz_file_t f);
void zzz_fclose(zzz_file_t f);
int64_t zzz_swrite(int fd, void *buf, int64_t size);
int64_t zzz_sread(int fd, void *buf, int64_t size);
void _zzz_entity_free(zzz_entity_t *ent);

/* crc32.c from zlib */
uint32_t crc32_z(uint32_t crc, const uint8_t *buf, uint64_t len);
uint32_t crc32_combine(uint32_t crc1, uint32_t crc2, uint64_t len2);

/* sha.c */
typedef struct {
   uint8_t d[64];
   uint32_t l;
   uint32_t b[2];
   uint32_t s[8];
} SHA256_CTX;
void SHA256_Init(SHA256_CTX *ctx);
void SHA256_Update(SHA256_CTX *ctx, const void *data, int len);
void SHA256_Final(SHA256_CTX *ctx, unsigned char *h);
void sha_init(zzz_ctx_t *zzz, uint8_t *password, int pwdlen);
void sha_reset(zzz_ctx_t *zzz, int i);
void sha_enc(zzz_ctx_t *zzz, int i, uint8_t *buf, uint64_t size);

/* aes.c */
void aes_init(zzz_ctx_t *zzz, uint8_t *password, int pwdlen);
void aes_reset(zzz_ctx_t *zzz, int i);
void aes_enc(zzz_ctx_t *zzz, int i, uint8_t *buf, uint64_t size);
void aes_dec(zzz_ctx_t *zzz, int i, uint8_t *buf, uint64_t size);
