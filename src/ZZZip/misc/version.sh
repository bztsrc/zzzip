#!/bin/sh

VERSION=`cat ../main.h|grep ZZZIP_VERSION|cut -d '"' -f 2`
COMMAVER=${VERSION//\./,}

cat <<EOF | sed 's/$/\r/g' >resource.rc
#include "../resource.h"
#include <windows.h>
#include <commctrl.h>

IDI_APP_ICON ICON "zzzip.ico"

VS_VERSION_INFO    VERSIONINFO
FILEVERSION        $COMMAVER,0
PRODUCTVERSION     $COMMAVER,0
FILEFLAGSMASK      VS_FFI_FILEFLAGSMASK
FILEFLAGS          0
FILEOS             VOS_NT_WINDOWS32
FILETYPE           VFT_APP
FILESUBTYPE        VFT2_UNKNOWN
BEGIN
  BLOCK "StringFileInfo"
  BEGIN
    BLOCK "080904b0"
    BEGIN
      VALUE "CompanyName", "bzt@gitlab"
      VALUE "FileDescription", "ZZZip"
      VALUE "FileVersion", "0.0.1"
      VALUE "InternalName", "Win32App"
      VALUE "LegalCopyright", "�2021 bzt@gitlab"
      VALUE "OriginalFilename", "zzzip.exe"
      VALUE "ProductName", "ZZZip by bzt"
      VALUE "ProductVersion", "$VERSION"
    END
  END
  BLOCK "VarFileInfo"
  BEGIN
    VALUE "Translation", 0x809, 1200
  END
END
EOF

cat <<EOF >zzzip.desktop
[Desktop Entry]
Version=$VERSION
Type=Application
Name=ZZZip
Comment=ZZZip archiver
Exec=/usr/bin/zzzip %u
Terminal=false
StartupNotify=false
X-MultipleArgs=true
Categories=Application;System;
Actions=create;extract;
[Desktop Action create]
Name=Create ZZZip archive
Exec=zzzip -c ? %F
[Desktop Action extract]
Name=Extract ZZZip archive
Exec=zzzip -e %F
EOF

cat <<EOF >deb_control
Package: zzzip
Version: $VERSION
Architecture: ARCH
Essential: no
Section: utils
Priority: optional
Maintainer: bzt
Homepage: https://gitlab.com/bztsrc/zzzip
Installed-Size: SIZE
Description: A very minimal tool to create and extract ZZZip archives
  Creates .zzz archives, and can extract .zzz, .zip, .7z, .tar, .cpio, .pax,
  .arj, .rar, .gz, .bz2, .xz, .zst archives without 3rd party dependencies.
EOF
