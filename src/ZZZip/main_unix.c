#include <stdio.h>
#include <sys/stat.h>
#include "main.h"

int main_isdir(char_t *path)
{
    struct stat st;
    return (!stat(path, &st) && S_ISDIR(st.st_mode));
}

int64_t main_filesize(char_t *path)
{
    struct stat st;
    return stat(path, &st) ? -1 : (int64_t)st.st_size;
}

uint64_t main_getfilemtime(char_t *path)
{
    struct stat st;
    return stat(path, &st) ? 0 : (uint64_t)st.st_mtime;
}

uint64_t main_parsedate(char_t *str)
{
    (void)str;
    return 0;
}
