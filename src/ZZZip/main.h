#include <stdint.h>

#define ZZZIP_VERSION "0.0.1"
#define ZZZIP_BUILD "1"

#define MODE_USAGE      0
#define MODE_CREATE     1
#define MODE_EXTRACT    2
#define MODE_LIST       3

#ifdef __WIN32__
#define FLAG "/"
#define CL(a) L ## a
#define main_fprintf wfprintf
typedef wchar_t char_t;
#else
#define FLAG "-"
#define CL(a) a
#define main_fprintf fprintf
typedef char char_t;
#endif

int main_isdir(char_t *path);
int64_t main_filesize(char_t *path);
uint64_t main_getfilemtime(char_t *path);
uint64_t main_parsedate(char_t *str);
