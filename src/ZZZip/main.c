#include <zzz.h>
#include <stdio.h>
#include "main.h"

void usage()
{
    main_fprintf(stdout, CL("ZZZip ") CL(ZZZIP_VERSION) CL(" (build ") CL(ZZZIP_BUILD)
        CL(") - MIT license, Copyright (C) 2021 bzt\r\n\r\n"));
}

int main(int argc, char_t **argv)
{
    int i, j, numarg = 0, mode = MODE_USAGE, all = 0, comp = 5;
    char lng[3] = "en";
    char_t *archive = NULL;
    uint64_t ft = 0;
    argv++;
    /* get command line flags */
    for(i = 1; i < argc && argv[0][0] == FLAG[0]; i++, argv++) {
        for(j = 1; argv[0][j]; j++)
            switch(argv[0][j]) {
                case CL('c'):
                    argv++;
                    if(!argv[0] || !argv[1]) { mode = MODE_USAGE; goto parsed; }
                    mode = MODE_CREATE;
                    archive = argv[0][0] == CL('?') && argv[0][1] == CL(0) ? NULL : *argv;
                    argv++;
                    numarg = argc - i - 2;
                    goto parsed;
                case CL('x'):
                    argv++;
                    if(!argv[0] || !argv[1]) { mode = MODE_USAGE; goto parsed; }
                    mode = MODE_EXTRACT;
                    archive = argv[0][0] == CL('?') && argv[0][1] == CL(0) ? NULL : *argv;
                    argv++;
                    numarg = 1;
                    goto parsed;
                case CL('l'):
                    if(!argv[1]) { mode = MODE_USAGE; goto parsed; }
                    mode = MODE_LIST;
                    archive = argv[1];
                    break;
                case CL('L'):
                    lng[0] = (char)argv[0][j + 1];
                    lng[1] = (char)argv[0][j + 2];
                    j += 2;
                    break;
                case CL('a'):
                    all = 1;
                    break;
                case CL('n'):
                    argv++; i++;
                    if( argv[0][0] >= CL('0') && argv[0][0] <= CL('9') &&
                        argv[0][1] >= CL('0') && argv[0][1] <= CL('9') &&
                        argv[0][2] >= CL('0') && argv[0][2] <= CL('9') &&
                        argv[0][3] >= CL('0') && argv[0][3] <= CL('9') &&
                        argv[0][4] == CL('-')) {
                            ft = main_parsedate(*argv);
                    } else
                        ft = main_getfilemtime(*argv);
                    break;
                case CL('0'): case CL('1'): case CL('2'): case CL('3'): case CL('4'):
                case CL('5'): case CL('6'): case CL('7'): case CL('8'): case CL('9'):
                    comp *= 10;
                    comp += argv[0][j] - CL('0');
                    break;
                default:
                    main_fprintf(stderr, CL("zzzip: unknown flag '%c'\r\n"), argv[0][j]);
                    return 1;
            }
    }
    /* if no flags given, autodetect mode from parameters in "cp" style */
    if(i + 1 < argc) {
        if(main_filesize(argv[0]) > 0 && main_isdir(argv[1])) {
            mode = MODE_EXTRACT;
            archive = *argv;
            argv++;
            numarg = 1;
        } else {
            numarg = argc - i - 1;
            for(i = 0; i + 1 < numarg && (main_filesize(argv[i]) >= 0 || main_isdir(argv[i])); i++);
            if(i + 1 == numarg && main_filesize(argv[i + 1]) == -1 && !main_isdir(argv[i + 1])) {
                archive = argv[i + 1];
                mode = MODE_CREATE;
            }
        }
    }
parsed:

    printf("mode %d all %d comp %d arhive '%s' numarg %d\n",mode,all,comp,archive,numarg);
    for(i = 0; i < numarg; i++)
        printf("input '%s'\n",argv[i]);

    switch(mode) {
        case MODE_USAGE:
            usage();
            break;
        case MODE_CREATE:
            break;
        case MODE_LIST:
            break;
        case MODE_EXTRACT:
            break;
    }
    return 0;
}
