#include <stdio.h>
#include <sys/stat.h>
#include "main.h"

int main_isdir(char_t *path)
{
    return GetFileAttributesW(path) & FILE_ATTRIBUTE_DIRECTORY ? 1 : 0;
}

int64_t main_filesize(char_t *path)
{
    FILE *f;
    int64_t size;
    if(!(f = _wfopen(path, "r"))) return -1;
    size = (int64_t)_filelengthi64(_fileno(f));
    fclose(f);
    return size;
}

uint64_t main_getfilemtime(char_t *path)
{
    HANDLE f = CreateFileW(path, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
    FILETIME ftC, ftA, ftM;
    if(f == INVALID_HANDLE_VALUE || !GetFiletime(f, &ftC, &ftA, &ftM)) return 0;
    CloseHandle(f);
    /* 1/100 nanosec, from 1601-01-01 */
    return (((uint64_t)fM.dwHighDateTime << 32) | (uint64_t)fM.dwLowDateTime) / UINT64_C(10000000) - UINT64_C(11644473600);
}

uint64_t main_parsedate(char_t *str)
{
    (void)str;
    return 0;
}
